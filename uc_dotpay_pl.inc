<?php

/**
 * @file
 * Author: Artur Kurek, artur at mate dot pl
 * http://mate.pl/.
 * Integrates dotpay.pl redirected payment service.
 * Drupal 6.x
 */

/**
 * Menu callback: settings form.
 */
function uc_dotpay_pl_settings_form() {
  $form = array();

  $form['donate'] = array(
    '#title' => t('Donate'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['donate']['info'] = array(
    '#value' => '<div style="float:right;margin:0 2px;"><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=3643175"><img src="https://www.paypal.com/pl_PL/PL/i/btn/btn_donate_LG.gif" style="border:0;margin:2px;padding:2px;" name="submit" alt="Dotacja"></a></div><div>'. t('If you want me to still develop Polish modules for Ubercart (incl. dotpay.pl and platnosci.pl), you need help or site updates please consider donating me. Thank you.') .'</div>',
  );

  $form['uc_dotpay_pl_var'] = array(
    '#title' => t('Variables'),
    '#type' => 'fieldset',
    '#description' => t('dotpay.pl system variables.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['uc_dotpay_pl_var']['uc_dotpay_pl_id'] = array(
    '#title' => t('User ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('uc_dotpay_pl_id', 0),
    '#description' => t('User account number displayed in administration panel, e.g. Logged in: 12345.'),
  );
  $form['uc_dotpay_pl_var']['uc_dotpay_pl_pin'] = array(
    '#title' => t('PIN'),
    '#type' => 'textfield',
    '#default_value' => variable_get('uc_dotpay_pl_pin', ''),
    '#description' => t('PIN available in URLC parameters.'),
  );
  $form['uc_dotpay_pl_var']['uc_dotpay_pl_onlinetransfer'] = array(
    '#title' => t('onlinetransfer'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('uc_dotpay_pl_onlinetransfer', ''),
    '#description' => t('onlinetransfer parameter.'),
  );


//  $form['uc_dotpay_pl_urls'] = array(
//    '#title' => t('Configuration URLs'),
//    '#type' => 'fieldset',
//    '#description' => t('Dotpay configuration URLs.'),
//    '#collapsible' => TRUE,
//    '#collapsed' => TRUE,
//  );
//
//  $form['uc_dotpay_pl_urls']['url_error'] = array(
//    '#value' => '<div><strong>'. t('URLC') .'</strong><br/>'. url('uc_dotpay_pl/online', array('absolute' => TRUE)) .'</div>',
//  );

  return system_settings_form($form);
}

/**
 * Menu callback
 */
function uc_dotpay_pl_payment_redirect() {
  if ($_POST['op'] == t('Back')) {
    drupal_goto('cart/checkout/review');
  }
  global $user;
  $url = 'https://ssl.dotpay.pl/?';

  $request = array();

  $order_id = @$_SESSION['cart_order'];
  if ($order_id) {
    uc_order_update_status($order_id, 'pending');
    $order = uc_order_load($order_id);
    //print dpm($order);

    $request['id']          = variable_get('uc_dotpay_pl_id', 0);
    $request['amount']      = $order->order_total;
    $request['currency']    = 'PLN';
    $request['description'] = t('!shop_name, nr !order_id',
      array(
        '!shop_name' => drupal_substr(variable_get('uc_store_name', $_SERVER['HTTP_HOST']), 0, 25),
        '!order_id' => $order->order_id,
    ));
    $request['lang']        = isset($user->language) ? $user->language : 'pl'; // @todo read from drupal config
    //$request['channel']     = 10;
    //ch_lock
    $request['onlinetransfer'] = variable_get('uc_dotpay_pl_onlinetransfer', 0);
    $request['control']     = $order_id;//_uc_dotpay_pl_get_control($order_id);
    $request['URL']         = url('uc_dotpay_pl/close_transaction/' . $request['control'], array('absolute' => TRUE));
    $request['type']        = 0;
    $request['buttontext']  = t('Return to shop');
    $request['URLC']        = url('uc_dotpay_pl/online', array('absolute' => TRUE));
    $request['firstname']   = $order->billing_first_name;
    $request['lastname']    = $order->billing_last_name;
    $request['email']       = $order->primary_email;
    $request['street']      = $order->billing_street1 .' '. $order->billing_street2;
    $request['street_n1']   = '';
    $request['street_n2']   = '';
    $request['state']       = '';
    $request['addr3']       = '';
    $request['city']        = $order->billing_city;
    $request['postcode']    = $order->billing_postal_code;
    $request['phone']       = $order->billing_phone;

    $billing_country        = uc_get_country_data(array('country_id' => $order->billing_country));
    $billing_country        = isset($billing_country[0]['country_iso_code_3']) ? $billing_country[0]['country_iso_code_3'] : 'POL';
    $request['country']     = $billing_country;
    //$request['code']      = '';

    $request['p_info']      = '';
    $request['p_email']     = '';

  }

  foreach ($request as $key => $val) {
    $url .= $key .'='. urlencode($val) .'&';
  }

  $transaction = $request;
  $transaction['uid'] = $user->uid;
  $transaction['order_id'] = $order_id;

  $transaction['status_id'] = 0;
  $transaction['ts'] = time();
  $transaction['ts_last_change'] = time();
  drupal_write_record('uc_dotpay_pl_transaction', $transaction);

  drupal_goto($url);
}

function uc_dotpay_pl_close_transaction() {
  $status = TRUE;
  $order_id = intval($_SESSION['cart_order']);
  $status_dotpay = isset($_GET['status']) ? $_GET['status'] : 'unknown';

  $watchdog_message = array();
  $watchdog_variables = array();
  $watchdog_link = t('view order: !view_order, dotpay: !view_in_dotpay', array(
    '!view_order' => l("#$order_id", 'admin/store/orders/'. $order_id .'/payments'),
    '!view_in_dotpay' => l('#'. $_POST['t_id'], _uc_dotpay_pl_details_url($_POST['t_id']))
  ));
  $watchdog_message[] = 'close transaction';
  $watchdog_message[] = 'GET: @var_get';
  $watchdog_variables['var_get'] = print_r($_GET, 1);
  $watchdog_message[] = 'POST: @var_post';
  $watchdog_variables['var_post'] = print_r($_POST, 1);

  if ($status_dotpay != 'OK') {
    $status = FALSE;
    $watchdog_message[] = 'dotpay error @status, TODO'; // TODO
    $watchdog_variables['@status'] = $_GET['status'];
  }
  if ($status_uc && !$order_id) {
    $status = FALSE;
    $watchdog_message[] = 'no order_id';
  }
//  if ($status && $control != _uc_dotpay_pl_get_control($order_id)) {
//    $status = FALSE;
//    $watchdog_message[] = 'invalid control';
//  }
  if ($status && !($order = uc_order_load($order_id))) {
    $status = FALSE;
    $watchdog_message[] = 'no order';
  }

  $severity = $status ? WATCHDOG_INFO : WATCHDOG_ERROR;

  watchdog('uc_dotpay_pl', implode($watchdog_message, "<br/>\n"), $watchdog_variables, $severity, $watchdog_link);

  if ($status) {
    // This lets us know it's a legitimate access of the complete page.
    $_SESSION['do_complete'] = TRUE;
    drupal_goto('cart/checkout/complete');
  }
  else {
    //return t('unknown error - todo');
    drupal_set_message(t('Payment Error Please contact Store Administrator'), 'warning');
    drupal_goto('cart');
  }
}

function _uc_dotpay_pl_load_transaction($key, $id) {
  $transaction = array();
  $result = db_query("SELECT * FROM {uc_dotpay_pl_transaction} t WHERE t.%s = '%s'", $key, $id);
  if ($transaction = db_fetch_array($result)) {
  }
  return $transaction;
}

function uc_dotpay_pl_online() {
  $status = TRUE;
  $order_id = $_POST['control'] ? $_POST['control'] : 0;
  $order = uc_order_load($order_id);
  $transaction = _uc_dotpay_pl_load_transaction('order_id', $order_id);

  $watchdog_message = array();
  $watchdog_variables = array();
  $watchdog_link = t('view order: !view_order, dotpay: !view_in_dotpay', array(
    '!view_order' => l("#$order_id", 'admin/store/orders/'. $order_id .'/payments'),
    '!view_in_dotpay' => l('#'. $_POST['t_id'], _uc_dotpay_pl_details_url($_POST['t_id']))
  ));
  $watchdog_message[] = 'URLC';
  $watchdog_message[] = 'GET: @var_get';
  $watchdog_variables['var_get'] = print_r($_GET, 1);
  $watchdog_message[] = 'POST: @var_post';
  $watchdog_variables['var_post'] = print_r($_POST, 1);

  // check
  if ($status && ! in_array($_SERVER["REMOTE_ADDR"], array('217.17.41.5', '195.150.9.37'))) {
    $status = FALSE;
    $watchdog_message[] = 'invalid REMOTE_ADDR: !REMOTE_ADDR';
    $watchdog_variables['!REMOTE_ADDR'] = $_SERVER["REMOTE_ADDR"];
  }
  if ($status && ! @$transaction['trid']) {
    $status = FALSE;
    $watchdog_message[] = 'no transaction';
  }
  if ($status && $order->order_total != $_POST['amount']) {
    $status = FALSE;
    $watchdog_message[] = 'wrong amount: !order_total <> @amount';
    $watchdog_variables['!order_total'] = $order->order_total;
    $watchdog_variables['@amount'] = $_POST['amount'];
  }
  $uc_md5 = md5(variable_get('uc_dotpay_pl_pin', 0) .':'.
              variable_get('uc_dotpay_pl_id', 0) .':'.
              @$_POST['control'] .':'.
              @$_POST['t_id'] .':'.
              @$_POST['amount'] .':'.
              @$_POST['email'] .':'.
              @$_POST['service'] .':'.
              @$_POST['code'] .':'.
              @$_POST['username'] .':'.
              @$_POST['password'] .':'.
              @$_POST['t_status']
  );
  if ($status && $uc_md5!=@$_POST['md5']) {
    $status = FALSE;
    $watchdog_message[] = 'invalid md5';
  }
  $old_status_id = $transaction['status_id'];
  $transaction['t_id'] = @$_POST['t_id'];
  $transaction['status_id'] = @$_POST['t_status'];
  if ($status && !drupal_write_record('uc_dotpay_pl_transaction', $transaction, 'trid')) {
    $status = FALSE;
    $watchdog_message[] = 'data update error';
  }

  if ($status) {
    switch ($old_status_id .'-'. @$_POST['t_status']) {
      case "0-2":
      case "1-2":
        $comment = t('view in dotpay: ') . l($_POST['t_id'], _uc_dotpay_pl_details_url($_POST['t_id']));
        uc_payment_enter($order_id, 'dotpay_pl', $order->order_total, $order->uid, '', $comment);
        $watchdog_message[] = 'uc_payment_enter: order #@order_id, @amount';
        $watchdog_variables['@order_id'] = $order_id;
        $watchdog_variables['@amount'] = $order->order_total;
        break;
      default:
        $watchdog_message[] = 'online order: #@order_id, amount: @amount, old status: @old_status_id, new status: @t_status';
        $watchdog_variables['@order_id'] = $order_id;
        $watchdog_variables['@amount'] = $order->order_total;
        $watchdog_variables['@old_status_id'] = _uc_dotpay_pl_get_status($old_status_id);
        $watchdog_variables['@t_status'] = _uc_dotpay_pl_get_status(@$_POST['t_status']);
        $change = array(t('Dotpay status') => array(
          'old' => _uc_dotpay_pl_get_status($old_status_id),
          'new' => _uc_dotpay_pl_get_status(@$_POST['t_status'])));
        uc_order_log_changes($order_id, $change);
        break;
    }
    print 'OK';
  }
  else {
    print t('ERROR, see drupal log');
  }

  $severity = ($status) ? WATCHDOG_INFO : WATCHDOG_ERROR;
  watchdog('uc_dotpay_pl', implode($watchdog_message, "<br/>\n"), $watchdog_variables, $severity, $watchdog_link);
}

function _uc_dotpay_pl_details_url($t_id) {
  return 'https://ssl.dotpay.pl/login.php?show=40&nt='. $t_id;
}

function _uc_dotpay_pl_get_status($status_id) {
  $types = _uc_dotpay_pl_status_types();
  return isset($types[$status_id]) ? $types[$status_id] : 'unkown';
}

function _uc_dotpay_pl_status_types() {
  return array(
    '0' => t('Not started'),
    '1' => t('New'),
    '2' => t('Payment received'),
    '3' => t('Rejected'),
    '4' => t('Cancelled'),
  );
}


